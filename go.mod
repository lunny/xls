module gitea.com/lunny/xls

go 1.12

require (
	github.com/extrame/goyymmdd v0.0.0-20181026012948-914eb450555b
	github.com/extrame/ole2 v0.0.0-20160812065207-d69429661ad7
	golang.org/x/text v0.3.2
)
